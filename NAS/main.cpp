#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <stdio.h> 
#include <string>
#include <cstdio>
#include <boost/concept_check.hpp>
#include <stdint.h>

using namespace std;

class data_parse
{
public:
  
  struct Data_Mem 
  {
    int address;
    uint16_t data[16];
  };

  struct blob
  {
    int id;
    const char *message;
    bool aliens_exist;
    uint16_t count;
    uint8_t option1;
    uint8_t option2;
    uint8_t option3;
    uint8_t option4;
  };
  
  void parsing();
  
};

void data_parse::parsing()
{
    ifstream file;
    file.open("testblob/blob4.dat",ios::in|ios::binary);
    string line;
    vector <string> strings;
    Data_Mem data_mem[100];
    
    string msg,sdata[16];
    int m=0;
    cout<<sizeof(msg)<<endl;
    
    //parse layer 1
    while(file.good())
    {
      getline(file,line,'\n');
      stringstream ss(line);
      string str = ss.str();
      strings.push_back(str); //blob content is also saved in vector
      if(str.size()>0)
      {
	//read address string and save address
	stringstream convert_add(str.substr(2,8));
	convert_add>>hex>>data_mem[m].address;
	cout<<"address: "<<data_mem[m].address<<endl;	
	
	//read data string and save data
	if(str.substr(14,1)==" ")
	{
	  for(int i=0;i<16;i++)
	  {
	  sdata[i]=str.substr(12+i*(2+1),2);
	  }
	}
	else if(str.substr(16,1)==" ")
	{
	  for(int i=0;i<2;i++)
	  {  
	    for(int j=0;j<7;j++)
	    {
	    sdata[i]=str.substr(12+2*i,2);
	    sdata[i+2*(j+1)]=str.substr(17+5*j+2*i,2);	      
	    }
	  }
	}
	else if(str.substr(20,1)==" ")
	{
	  for(int i=0;i<4;i++)
	  {  
	    sdata[i]=str.substr(12+2*i,2);
	    sdata[i+4]=str.substr(21+2*i,2);
	    sdata[i+8]=str.substr(30+2*i,2);
	    sdata[i+12]=str.substr(39+2*i,2);  
	  }
	}	
	cout<<"data: ";		
	for(int i=0;i<16;i++)
	{
	  stringstream convert_data(sdata[i]);
	  convert_data>>hex>>data_mem[m].data[i];	 
	  cout<<data_mem[m].data[i]<<","; 
	}
	 cout<<endl;
      }
      m++;
    }
   
    //parse layer 2
    blob read_blob[strings.size()];
    
    for (unsigned i=0; i<strings.size(); i++)
      { 
	//messages	
	read_blob[i].message = "This is message";
	cout<<"message: "<<read_blob[i].message<<endl;
	
	//id
	read_blob[i].id=i;
	cout<<"id: "<<read_blob[i].id<<endl;
	
	//data
	uint16_t shift=data_mem[i].data[0];
	read_blob[i].aliens_exist=((shift >> 7)  & 0x01);
	cout<<"aliens_exist: "<<read_blob[i].aliens_exist<<endl;
			
	int temp = data_mem[i].data[0] << 8;
	read_blob[i].count=data_mem[i].data[1] & temp;
	read_blob[i].count &= ~(1 << 15);
	cout<<"count: "<<(data_mem[i].data[0])<<endl;
	
	//option	
	//do option assignment
	
	cout<<""<<endl;
      }
}

int main() 
{
    data_parse data_parse_blob;
    data_parse_blob.parsing();
    
}


